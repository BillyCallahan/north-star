export default class Coordinates {
	coords: GeolocationCoordinates;

	constructor(c: GeolocationCoordinates) {
		this.coords = c;
	}

	getAngleToTarget(pointLatitude: number, pointLongitude: number): number {
		const phiK = (pointLatitude * Math.PI) / 180.0;
		const lambdaK = (pointLongitude * Math.PI) / 180.0;
		const phi = (this.coords.latitude * Math.PI) / 180.0;
		const lambda = (this.coords.latitude * Math.PI) / 180.0;
		const psi =
			(180.0 / Math.PI) *
			Math.atan2(
				Math.sin(lambdaK - lambda),
				Math.cos(phi) * Math.tan(phiK) - Math.sin(phi) * Math.cos(lambdaK - lambda)
			);
		return Math.round(psi);
	}
}
