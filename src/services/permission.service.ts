export default class PermissionService {

	static async requestDeviceOrientationAccess(): Promise<void> {
		const isIOS =
			navigator.userAgent.match(/(iPod|iPhone|iPad)/) && navigator.userAgent.match(/AppleWebKit/);

		if (isIOS) {
			try {
				const permission = await DeviceOrientationEvent.requestPermission();

				if (permission !== 'granted')
					throw new Error("Without device orientation access, you won't find your way.");
			} catch {
				throw new Error('Your device does not support orientation sensors.');
			}
		}
	}
}
